package com.maxim.classwork10;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.maxim.classwork10.Utils.isExternalStorageWritable;

public class MainActivity extends AppCompatActivity {

    public static final String DIR_NAME = "/MyDir";
    public static final String FILE_NAME = "/MyDir";

    @BindView(R.id.am_login)
    EditText login;
    @BindView(R.id.am_password)
    EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        loadIdExists();

        if (!getPermissions()) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //resume tasks needing this permission
        }
    }

    private void loadIdExists() {
        //todo
    }

    @OnClick(R.id.am_action_save)
    void onSAveClick() {
        if (isExternalStorageWritable()) {
            //create dirs
            File sdCard = Environment.getExternalStorageDirectory();
            File directory = new File(sdCard.getAbsolutePath() + "/" + DIR_NAME);
            directory.mkdirs();

            //create file
            File file = new File(directory, FILE_NAME);

            try {
                //write data to file
                FileOutputStream fOut = new FileOutputStream(file);
                OutputStreamWriter osw = new OutputStreamWriter(fOut);
                osw.write(login.getText().toString());
                osw.write("\n");
                osw.write(password.getText().toString());
                osw.flush();
                osw.close();
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean getPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }

    }
}
